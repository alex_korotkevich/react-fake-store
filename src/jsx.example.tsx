import { createElement as e, useState } from "react";

function App() {
  const [count, setCount] = useState(0);
  // return (
  //   // <div className="App">
  //   //   <header className="App-header">
  //   //     <img src={logo} className="App-logo" alt="logo" />
  //   //     <p>
  //   //       Edit <code>src/App.tsx</code> and save to reload.
  //   //     </p>
  //   //     <a
  //   //       className="App-link"
  //   //       href="https://reactjs.org"
  //   //       target="_blank"
  //   //       rel="noopener noreferrer"
  //   //     >
  //   //       Learn React
  //   //     </a>
  //   //   </header>
  //   // </div>
  //   <h1>HELLO</h1>
  // );
  return e("div", { className: "container" }, [
    e("h1", { className: "font-bold", key: "1" }, "Test JSX"),
    e(
      "button",
      {
        className: "py-2 px-4 border",
        key: "2",
        onClick: () => {
          setCount(count + 1);
        },
      },
      "Click Me!"
    ),
    e(
      "div",
      {
        className: "mt-6",
        key: "3",
      },
      count
    ),
  ]);
}

export default App;
