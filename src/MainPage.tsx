import React, { useState } from "react";
import { CreateProduct } from "./components/CreateProduct";
import { ErrorMessage } from "./components/ErrorMessage";
import { Loader } from "./components/Loader";
import { Modal } from "./components/Modal";
import { Product } from "./components/Product";
import { IProduct } from "./models";
import { useProducts } from "./hooks/products";
import { useNavigate } from "react-router-dom";

export const MainPage = () => {
  const [modal, setModal] = useState<boolean>(true);
  const { products, loading, error, addProduct } = useProducts();

  const navigate = useNavigate();

  const toNewPage = () => {
    navigate("/test");
  };

  const createHandler = (product: IProduct) => {
    setModal(false);
    addProduct(product);
  };

  const closeModal = () => {
    setModal(false);
  };

  return (
    <div className="container mx-auto pt-5 max-w-2xl">
      <button onClick={toNewPage} className="p-5 border">
        To page 1
      </button>
      {loading && <Loader />}
      {error && <ErrorMessage error={error} />}
      {products.map((prod: IProduct) => (
        <Product product={prod} key={prod.id} />
      ))}
      {modal && (
        <Modal title="Create new product">
          <CreateProduct onCreate={createHandler} />
          <button onClick={closeModal}>Close Modal</button>
        </Modal>
      )}
    </div>
  );
};
