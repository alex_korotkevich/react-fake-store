import React, { useState } from "react";
import { IProduct } from "../models";

interface CreateFullProductProps {
  onChanges: (product: IProduct) => void;
}

export const CreateFullProduct = ({ onChanges }: CreateFullProductProps) => {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");

  const productData: IProduct = {
    title: "",
    price: 0,
    description: "lorem ipsum set",
    image: "https://i.pravatar.cc",
    category: "",
    rating: {
      rate: 0,
      count: 0,
    },
  };

  const submitHandler = (e: React.FormEvent) => {
    e.preventDefault();
    onChanges(productData);
  };

  return (
    <form onSubmit={submitHandler}>
      <input
        type="text"
        className="border py-2 px-4 mb-2 outline-0 w-full"
        placeholder="Enter Product Title..."
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />
      <input
        type="number"
        className="border py-2 px-4 mb-2 outline-0 w-full"
        placeholder="Enter Product Price..."
        value={price}
        onChange={(e) => setPrice(+e.target.value)}
      />
      <input
        type="text"
        className="border py-2 px-4 mb-2 outline-0 w-full"
        placeholder="Enter Product Description..."
        value={description}
        onChange={(e) => setDescription(e.target.value)}
      />
      <input
        type="text"
        className="border py-2 px-4 mb-2 outline-0 w-full"
        placeholder="Enter Product Category..."
        value={category}
        onChange={(e) => setCategory(e.target.value)}
      />

      {/* {error && <ErrorMessage error={error} />} */}
      <button type="submit" className="border py-2 px-4 bg-yellow-400 hover:text-white">
        Create
      </button>
    </form>
  );
};
