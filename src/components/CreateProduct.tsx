import axios from "axios";
import React, { useState } from "react";
import { IProduct } from "../models";
import { ErrorMessage } from "./ErrorMessage";

interface CreateProductsProps {
  onCreate: (product: IProduct) => void;
}

export const CreateProduct = ({ onCreate }: CreateProductsProps) => {
  const [value, setValue] = useState("");
  const [error, setError] = useState("");

  const productData: IProduct = {
    title: "",
    price: 13.5,
    description: "lorem ipsum set",
    image: "https://i.pravatar.cc",
    category: "electronic",
    rating: {
      rate: 0,
      count: 0,
    },
  };

  const submitHandler = async (event: React.FormEvent) => {
    event.preventDefault();
    setError("");

    if (value.trim().length === 0) {
      setError("Please enter valid title");
      return;
    }

    productData.title = value;

    const response = await axios.post<IProduct>("https://fakestoreapi.com/products", productData);

    onCreate(response.data);
  };

  //React.KeyboardEvent<HTMLInputElement>
  const changeHandler = (event: any) => {
    setValue(event.target.value);
  };

  return (
    <form onSubmit={submitHandler}>
      <input
        type="text"
        className="border py-2 px-4 mb-2 outline-0 w-full"
        placeholder="Enter Product Title..."
        value={value}
        onChange={changeHandler}
      />
      {error && <ErrorMessage error={error} />}
      <button type="submit" className="border py-2 px-4 bg-yellow-400 hover:text-white">
        Create
      </button>
    </form>
  );
};
