import React, { useState } from "react";
import { Modal } from "./components/Modal";
import { CreateFullProduct } from "./components/CreateFullProduct";
import { Product } from "./components/Product";
import { IProduct } from "./models";

export const TestPage = () => {
  const [modal, setModal] = useState<boolean>(false);
  const [products, setProducts] = useState<IProduct[]>([
    {
      title: "Default Product",
      price: 100500,
      description: "lorem ipsum set",
      image: "https://i.pravatar.cc",
      category: "",
      rating: {
        rate: 0,
        count: 0,
      },
    },
  ]);

  const openForm = () => {
    setModal(true);
  };

  const addProduct = (product: IProduct) => {
    setProducts((prew) => [...prew, product]);
    setModal(false);
  };
  console.log("");
  return (
    <>
      <button onClick={openForm} className="border py-2 px-4 bg-yellow-400">
        Create Product
      </button>
      {modal && (
        <Modal>
          <CreateFullProduct onChanges={addProduct} />
        </Modal>
      )}
      {products.map((prod: IProduct, index) => (
        <Product product={prod} key={index} />
      ))}
    </>
  );
};
